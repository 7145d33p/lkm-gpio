//modified from Derek Molloy's examples
//see http://www.derekmolloy.ie/

#include <linux/init.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/time.h>		//for time stamping interrupt
#include <linux/interrupt.h>
#include <linux/gpio.h>

//char driver related
#define  DEVICE_NAME "pd2627"    // /dev/pd2627
#define  CLASS_NAME  "pdgpio"    //

static int    majorNumber;                  // Stores the device number
#define MESSAGE_SIZE	256
static char message[MESSAGE_SIZE] = {0};
static short  size_of_message;
static int    numberOpens = 0;              // Counts the number of times the device is opened
static struct class*  pd2627Class  = NULL; // The device-driver class struct pointer
static struct device* pd2627Device = NULL; // The device-driver device struct pointer

// must come before the struct definition
static int     dev_open(struct inode *, struct file *);
static int     dev_release(struct inode *, struct file *);
static ssize_t dev_read(struct file *, char *, size_t, loff_t *);
static ssize_t dev_write(struct file *, const char *, size_t, loff_t *);

static struct file_operations fops =
{
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

//gpio related
static unsigned int gpioPD26 = 0x60+26;	//PD26's gpio number. input
static unsigned int gpioPD27 = 0x60+27; //PD27's gpio number. output
static unsigned int irqNumber;		//irq number assigned
static unsigned int irqcount = 0;

static irq_handler_t GPIOlkm_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs);

static int __init pd2627_init(void){
  int result = 0;

   printk(KERN_INFO "pd2627: init LKM\n");
  
   // Try to dynamically allocate a major number for the device -- more difficult but worth it
   majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
   if (majorNumber<0){
      printk(KERN_ALERT "pd2627 failed to register a major number\n");
      return majorNumber;
   }
   printk(KERN_INFO "pd2627: registered correctly with major number %d\n", majorNumber);

   // Register the device class
   pd2627Class = class_create(THIS_MODULE, CLASS_NAME);
   if (IS_ERR(pd2627Class)){                // Check for error and clean up if there is
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to register device class\n");
      return PTR_ERR(pd2627Class);          // Correct way to return an error on a pointer
   }
   printk(KERN_INFO "pd2627: device class registered correctly\n");

   // Register the device driver
   pd2627Device = device_create(pd2627Class, NULL, MKDEV(majorNumber, 0), NULL, DEVICE_NAME);
   if (IS_ERR(pd2627Device)){               // Clean up if there is an error
      class_destroy(pd2627Class);           // Repeated code but the alternative is goto statements
      unregister_chrdev(majorNumber, DEVICE_NAME);
      printk(KERN_ALERT "Failed to create the device\n");
      return PTR_ERR(pd2627Device);
   }
   printk(KERN_INFO "pd2627: device class created correctly\n");
   
   printk(KERN_INFO "pd2627: requesting GPIO and IRQ number\n");
  
   gpio_request(gpioPD27, "sysfs"); 	  //wow. very familar
   gpio_direction_output(gpioPD27, true); //set output
   gpio_export(gpioPD27, false);	  //false = no change in direction
   gpio_request(gpioPD26, "sysfs");
   gpio_direction_input(gpioPD26);
   gpio_set_debounce(gpioPD26, 200);	 //200ms debounce delay
   gpio_export(gpioPD26, false);
   
   printk(KERN_INFO "pd2627: PD26 is : %d\n", gpio_get_value(gpioPD26));
   irqNumber = gpio_to_irq(gpioPD26);
   printk(KERN_INFO "pd2627: PD26 irq is : %d\n", irqNumber);
   
   result = request_irq(irqNumber, (irq_handler_t) GPIOlkm_irq_handler,
                       IRQF_TRIGGER_RISING, 
                       "GPIOlkm_handler", NULL);  // /proc/interrupts
  printk(KERN_INFO "pd2627: Interrupt assignment : %d\n", result);
  return result;
}

static void __exit pd2627_exit(void){
   //releasing all gpio stuff
   printk(KERN_INFO "pd2627: interrupt count : %d\n", irqcount);
   gpio_unexport(gpioPD27);
   free_irq(irqNumber, NULL);	//release it
   gpio_unexport(gpioPD26);
   gpio_free(gpioPD27);
   gpio_free(gpioPD26);
   //unregister char device driver
   printk(KERN_INFO "pd2627: unregistering char device driver\n");
   device_destroy(pd2627Class, MKDEV(majorNumber, 0));     // remove the device
   class_unregister(pd2627Class);                          // unregister the device class
   class_destroy(pd2627Class);                             // remove the device class
   unregister_chrdev(majorNumber, DEVICE_NAME);             // unregister the major number
   printk(KERN_INFO "pd2627: Goodbye!\n");
}

static int dev_open(struct inode *inodep, struct file *filep){
   numberOpens++;
   printk(KERN_INFO "pd2627: Device has been opened %d time(s)\n", numberOpens);
   return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset){
   int error_count = 0;
   int res =0;
   // copy_to_user has the format ( * to, *from, size) and returns 0 on success
   error_count = copy_to_user(buffer, message, size_of_message);

   if (error_count==0){            // if true then have success
      //printk(KERN_INFO "pd2627: Sent %d characters to the user\n", size_of_message);
      res = size_of_message;
      size_of_message = 0;
      return (res);  // clear the position to the start and return 0
   }
   else {
      printk(KERN_INFO "pd2627: Failed to send to the user\n");
      return -EFAULT;              // Failed -- return a bad address message (i.e. -14)
   }
}

static ssize_t dev_write(struct file *filep, const char *buffer, size_t len, loff_t *offset){
   size_t i;
   for (i=0; i<len; i++) {
		if (buffer[i] == '0') gpio_set_value(gpioPD27, 0);
		else gpio_set_value(gpioPD27, 1);
   }
   return (len);
}

static int dev_release(struct inode *inodep, struct file *filep){
   printk(KERN_INFO "pd2627: Device successfully closed\n");
   return 0;
}

static irq_handler_t GPIOlkm_irq_handler(unsigned int irq, void *dev_id, struct pt_regs *regs) {
  struct timespec ts;

  //gpio_set_value(gpioPD27, 1);	//mark the isr timing
  //gpio_set_value(gpioPD27, 0);
  //gpio_set_value(gpioPD27, 1);
  //gpio_set_value(gpioPD27, 0);
  //printk(KERN_INFO "GPIOlkm: IRQ : %d\n", gpio_get_value(gpioPD26));
  getnstimeofday(&ts);	//get current time
  sprintf(message, "%08X ", (unsigned int)(ts.tv_sec));
  size_of_message = 9;
  irqcount++;
  //gpio_set_value(gpioPD27, 0);	//mark the isr timing
  return (irq_handler_t) IRQ_HANDLED;
}
module_init(pd2627_init);
module_exit(pd2627_exit);
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("gpio driver");

